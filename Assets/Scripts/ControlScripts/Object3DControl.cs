﻿using UnityEngine;
using System.Collections;
using Jupitech.Controllers;
using UnityEngine.EventSystems;

public class Object3DControl : MonoBehaviour {

	float mousex=0,mousey=0;

	Vector3 mouseposition;
	public bool isOn =true;
	public LayerMask layerTerrain;
	public LayerMask layer3DObject;
	public Camera myCamera;
	public PanelGroupItem panelGroupItemScroll;

	GameObject objectForAction; //object duoc tao sau khi chon

	Vector2 postouch=Vector2.zero; //vi tri chuot luc xoay


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if(isOn){
		Debug.Log(Utils.stateSelect);

		//object khi chon
		if(Input.GetMouseButtonDown(0)){
			if(EventSystem.current.IsPointerOverGameObject()){
				return;
			}
			Ray ray=myCamera.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if(Physics.Raycast(ray,out hit,Mathf.Infinity,layer3DObject))
			{
//				Debug.Log (hit.collider.gameObject.name);
//				Debug.Log(hit.point);
//				objectForAction=hit.collider.gameObject;

				if(Utils.stateSelect!=Constrains.STATE_OBJECT3D_SELECT_ROTATE){
					objectForAction=hit.collider.gameObject;
				}

				//move object
				if(Utils.stateSelect==Constrains.STATE_OBJECT3D_SELECT_MOVE){
					objectForAction=hit.collider.gameObject;
				}
			}

			//vi tri chuot down luc xoay
			if(Utils.stateSelect==Constrains.STATE_OBJECT3D_SELECT_ROTATE || Utils.stateSelect==Constrains.STATE_OBJECT3D_LONGSELECT){
				postouch=Input.mousePosition;
			}

		}

		//touch up
		if(Input.GetMouseButtonUp(0)){

			//click model
			if(Utils.stateSelect!=Constrains.STATE_OBJECT3D_MOVE){
				//action vao model
				Ray ray=myCamera.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if(Physics.Raycast(ray,out hit,Mathf.Infinity,layer3DObject))
				{
					
						if(objectForAction.tag==Constrains.TAG_MODEL_TOY){
						
						//ACTION slider Bar
						ModelToyObject modelToy=objectForAction.GetComponent<ModelToyObject>();
						modelToy.ActionForcusModel();
						Debug.Log("select");
					}
					
				}
			}


			if(Utils.stateSelect==Constrains.STATE_OBJECT3D_MOVE || Utils.stateSelect==Constrains.STATE_OBJECT3D_LONGSELECT){
				Utils.stateSelect=Constrains.STATE_OBJECT_RESET;
			}

			if(Utils.stateSelect==Constrains.STATE_OBJECT3D_SELECT_MOVE){
				objectForAction=null;

			}

		}
		if(Input.GetMouseButton(0)){

			//di chuyen object
			if(objectForAction!=null){
				if(Utils.stateSelect==Constrains.STATE_OBJECT3D_SELECT_MOVE && objectForAction.tag==Constrains.TAG_MODEL_TOY){
					Ray ray=myCamera.ScreenPointToRay(Input.mousePosition);
					RaycastHit hit;
					if(Physics.Raycast(ray,out hit,Mathf.Infinity,layerTerrain))
					{
						Vector3 hittemp=new Vector3(hit.point.x,hit.point.y,hit.point.z);

						ModelToyObject modelToy=objectForAction.GetComponent<ModelToyObject>();
						modelToy.Move(hittemp);
						
					}
				}
			}

			//vi tri chuot down luc xoay
			if(Utils.stateSelect==Constrains.STATE_OBJECT3D_SELECT_ROTATE){
				if(objectForAction!=null){

					ModelToyObject modelToy=objectForAction.GetComponent<ModelToyObject>();

					if(Input.mousePosition.x<postouch.x){
						
						modelToy.RotateLeft(true,Time.deltaTime*(Input.mousePosition.x-postouch.x));
					}else if(Input.mousePosition.x>postouch.x){

						modelToy.RotateLeft(false,Time.deltaTime*(Input.mousePosition.x-postouch.x));
					}
				}

			}

		}

		//khi keo object vao terrain moi bat dau khoi tao object
		if(Utils.stateSelect==Constrains.STATE_OBJECT2D_SELECT){
			Ray ray=myCamera.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if(Physics.Raycast (ray,out hit,Mathf.Infinity,layerTerrain))
			{
				
				Vector3 hittemp=new Vector3(hit.point.x,hit.point.y,hit.point.z);
//				Debug.Log("hit terrain");
				Utils.stateSelect=Constrains.STATE_OBJECT3D_MOVE;
				DataAppControl.Instance.setNumber(Utils.objectSelect,DataAppControl.Instance.getNumber(Utils.objectSelect)-1);
				objectForAction = Instantiate(DataAppControl.Instance.getModel(Utils.objectSelect),Input.mousePosition,Quaternion.identity) as GameObject;
				panelGroupItemScroll.ReloadNumItemCanvasToy(); //reload lai scrollview

			}
		}
		//dich chuyen object khi vua keo vao terrain
		if(Utils.stateSelect==Constrains.STATE_OBJECT3D_MOVE){

			Ray ray=myCamera.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
//			Debug.Log(Mathf.Infinity);
			if(Physics.Raycast (ray,out hit,Mathf.Infinity,layerTerrain))
			{

				Vector3 hittemp=new Vector3(hit.point.x,hit.point.y,hit.point.z);
				objectForAction.transform.position=hittemp;
			}
		}
		
			
	}
	}

}
