﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ListSubrb : MonoBehaviour
{
    [SerializeField]
    private GameObject listSelect;
    [SerializeField]
    private Text _textShow;
    public bool isShowList;
    public void ShowList()
    {
        isShowList = !isShowList;
        if (isShowList)
            listSelect.SetActive(true);
        else
        {
            listSelect.SetActive(false);
        }

    }
    public  void ChangeText(string txt)
    {
        _textShow.text = txt;
    }
	
}
