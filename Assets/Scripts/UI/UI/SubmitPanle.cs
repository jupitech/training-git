﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SubmitPanle : MonoBehaviour {

    [SerializeField]
    private GameObject _panlesubmit;
    public void ShowPanleSubmit()
    {
		AnimPanelControl anim=gameObject.GetComponent<AnimPanelControl>();
		anim.showPanel();

        _panlesubmit.SetActive(true);
    }
    public void HidenPanleSubmit()
    {
		AnimPanelControl anim=gameObject.GetComponent<AnimPanelControl>();
		anim.hidePanel();

        _panlesubmit.SetActive(false);
    }
	
}
