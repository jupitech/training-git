﻿using UnityEngine;
using System.Collections;

public class ScrollviewItem : MonoBehaviour {

	public GameObject scrollviewitem;
	public GameObject background;

	// Use this for initialization
	void Start () {
		
	}
	
	public void setActionScrollView(){
		scrollviewitem.SetActive(true);
		background.SetActive(true);
	}
	public void setDisableScrollView(){
		scrollviewitem.SetActive(false);
		background.SetActive(false);
	}
}
