﻿using UnityEngine;
using System.Collections;
using Jupitech.Controllers;

public class CanvasObjectSelect : MonoBehaviour {

	public CanvasModelToy canvasModelToy;
	public PanelButton panelButton;

	ModelToyObject modelToyObject;
	
	public JExteriorPlayer exteriorPlayer;
	
	public GameObject psChoise;

	GameObject psChoiseTemp;


	// Use this for initialization
	void Start () {
	
	}
	
	public void setDisable(){
		panelButton.setDisableButtonAction();
		panelButton.setDisableButtonConfig();
	}


	public void setActionModelObject(ModelToyObject modelToyObj){
		this.modelToyObject=modelToyObj;
		panelButton.setActionButtonAction();
		panelButton.setActionButtonConfig();
		canvasModelToy.setAction(modelToyObj.gameObject.transform.position);
	}

	public void Button_Rotate(){
		
		modelToyObject.ActionForcusRotate=true;
		//		Debug.Log("button_rotate "+gameObject.name);
		modelToyObject.setInitRotate();
		
//		DisableButtonAction();
//		scrollView.setDisableScrollView();
		
		Utils.stateSelect=Constrains.STATE_OBJECT3D_SELECT_ROTATE;
		
		if(modelToyObject.ActionForcus){
			canvasModelToy.State_Rotate();
		}
	}
	public void Button_Move(){
		
		modelToyObject.ActionForcusMove=true;
		modelToyObject.setInitMove();
//		scrollView.setDisableScrollView();
		
		Utils.stateSelect=Constrains.STATE_OBJECT3D_SELECT_MOVE;
		
		if(modelToyObject.ActionForcus){
			canvasModelToy.State_Move();
		}
	}
	public void Button_ChangeObj(){
//		scrollView.setActionScrollView();
	}
	
	public void Button_Cancel(){
		Debug.Log("cancel");
		if(modelToyObject.ActionForcusMove){
			modelToyObject.loadInitMove();
		}
		if(modelToyObject.ActionForcusRotate){
			modelToyObject.loadInitRotate();
		}
		
		CancelUI();
	}
	public void Button_Config(){
		CancelUI();
	}
	public void Button_DeleteModel(){
		DataAppControl.Instance.setNumber(Utils.objectSelect,DataAppControl.Instance.getNumber(Utils.objectSelect)+1);
		
		modelToyObject.DeleteModel();
		CancelUI();
	}
	
	public void actionUI(){

		exteriorPlayer.setGameObjectSelect(modelToyObject.gameObject);
		
		psChoiseTemp= Instantiate(psChoise,transform.position,Quaternion.identity) as GameObject;
	}
	
	void CancelUI(){
		exteriorPlayer.setGameObjectSelect(null);
		modelToyObject.ActionForcus=false;
		modelToyObject.ActionForcusMove=false;
		modelToyObject.ActionForcusRotate=false;
//		scrollView.setDisableScrollView();
		canvasModelToy.setDisable();
		setDisable();
		
		Utils.stateSelect=Constrains.STATE_OBJECT_RESET;
		
		Destroy(psChoiseTemp);
	}
	public void ChangeObj(int id){
		
		modelToyObject.ChangeObjModel(id);
		CancelUI();
		
	}

}
