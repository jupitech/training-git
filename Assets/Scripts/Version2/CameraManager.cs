using UnityEngine;
using System.Collections;
using Jupitech.Controllers;
using UnityEngine.UI;
using System.Collections.Generic;
public class CameraManager : MonoBehaviour {

    public List<Button> _listbutton;
  
    private JExteriorPlayer _camera;
    private Object3DControl _model;
	// Use this for initialization
	void Start ()
    {
        _listbutton = new List<Button>();
        _model = FindObjectOfType<Object3DControl>();
	    _camera=FindObjectOfType<JExteriorPlayer>();

	}

    public void CameraEdit()
    {


        _camera.isOn = false;
        _model.isOn = true;
    }
    public void CameraView()
    {
        _camera.isOn = true;
        _model.isOn = false;
    }

    //void SetAllDefault()
    //{
    //    if(_listbutton!=null)
    //    for (int i = 0; i < _listbutton.Count; i++)
    //    {
    //        _listbutton[i].image.color = Color.white;
    //    }
    //}
  
	
	
}
