﻿using UnityEngine;
using System.Collections;

public class DataAppControl : MonoBehaviour {

	//cac chi so doi xung nhau

	public int[] lstID;
	public int[] lstNumber;
	public Sprite[] lstSprite;
	public GameObject[] lstModel;

	int[] initLstNumber;

	private static DataAppControl _instance;
	
	public static DataAppControl Instance
	{
		get
		{
			return _instance;
		}
	}
	
	void Awake()
	{
		if (_instance == null)
		{
			_instance = this;
			
			DontDestroyOnLoad(this);
		}
		else
		{
			DestroyImmediate(this);
		}
	}
	void Start(){
		initLstNumber=new int[lstNumber.Length];
		for(int i=0;i<initLstNumber.Length;i++){
			initLstNumber[i]=lstNumber[i];
		}
	}

	public Sprite getSprite(int id){
		return lstSprite[id];
	}
	public GameObject getModel(int id){
		return lstModel[id];
	}
	public void setNumber(int id,int num){
		lstNumber[id]=num;
	}
	public int getNumber(int id){
		return lstNumber[id];
	}

	public void reloadNumber(){
		for(int i=0;i<lstNumber.Length;i++){
			lstNumber[i]=initLstNumber[i];
		}
	}

}
