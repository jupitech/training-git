﻿using UnityEngine;
using System.Collections;

public class ButtonSample : MonoBehaviour {

	[SerializeField]
	private GameObject _panleSample;
    public void Click(int id)
    {

        for (int i = 0; i < transform.GetChildCount(); i++)
        {
            transform.GetChild(i).gameObject.transform.GetChild(0).gameObject.SetActive(false);
        }
        transform.GetChild(id).transform.GetChild(0).gameObject.SetActive(true);
		_panleSample.SetActive (false);
		Godthanh.Instance._button.isShowSample=false;
        Debug.Log("OK");
    }
}
