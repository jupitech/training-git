﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemCanvasHouse : MonoBehaviour
,IPointerExitHandler,IPointerEnterHandler
,IPointerUpHandler,IPointerDownHandler {



	public int idModel=0;

	Vector3 scaleInit;
	Vector3 posInit;

	ScrollViewControl scrollViewControl;
	Text txtNum;


	// Use this for initialization
	void Start () {
		scaleInit=transform.localScale;
		posInit=transform.position;
		scrollViewControl=transform.parent.parent.GetComponent<ScrollViewControl>();
		txtNum=gameObject.GetComponentInChildren<Text>();
		updateNumberToy();
	}

	#region IPointerExitHandler implementation

	public void OnPointerExit (PointerEventData eventData)
	{
		if(DataAppControl.Instance.getNumber(idModel)>0){
			
			//		Debug.Log("mouse exit");
			transform.localScale=scaleInit;
		}
	}

	#endregion

	#region IPointerEnterHandler implementation

	public void OnPointerEnter (PointerEventData eventData)
	{
		if(DataAppControl.Instance.getNumber(idModel)>0){
			
			//		Debug.Log("mouse enter");
			transform.localScale=new Vector3(0.9f,0.9f,0.9f);
		}
	}

	#endregion

	#region IPointerUpHandler implementation

	public void OnPointerUp (PointerEventData eventData)
	{
		transform.localScale=scaleInit;
//		resetPosition();
		scrollViewControl.setScrollViewEnable();
	}

	#endregion
	

	#region IPointerDownHandler implementation

	public void OnPointerDown (PointerEventData eventData)
	{
		if(Utils.stateSelect==Constrains.STATE_OBJECT3D_MOVE
		   || Utils.stateSelect==Constrains.STATE_OBJECT3D_SELECT_MOVE
		   || Utils.stateSelect==Constrains.STATE_OBJECT3D_SELECT_ROTATE
		   || Utils.stateSelect==Constrains.STATE_OBJECT3D_CHOISE_BUTTON){
			return;
		}
		if(DataAppControl.Instance.getNumber(idModel)>0){

			Utils.stateSelect=Constrains.STATE_OBJECT2D_SELECT;
			
//			Debug.Log("id model"+idModel);
			Utils.objectSelect=idModel;
			scrollViewControl.setScrollViewDisable();
		}

	}

	#endregion

	public void resetPosition(){
		Utils.stateSelect=Constrains.STATE_OBJECT_RESET;
	}
	public void DesNumber(){
		//tru so luong model
		if(Utils.objectSelect==idModel){
			DataAppControl.Instance.setNumber(idModel,DataAppControl.Instance.getNumber(idModel)-1);
			txtNum.text=DataAppControl.Instance.getNumber(idModel)+"";
		}
	}
	public void updateNumberToy(){
		txtNum.text=DataAppControl.Instance.getNumber(idModel)+"";
	}
}
