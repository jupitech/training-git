﻿using UnityEngine;
using System.Collections;

public class PanelButton : MonoBehaviour {

	public GameObject btnMove,btnRotate,btnConfig,btnCancel,btnChangeObj,btnDelete;
	// Use this for initialization
	void Start () {
		
	}


	/// <summary>
	/// 2 loai button di chuyen, xoay
	/// </summary>
	public void setActionButtonAction(){
		btnMove.SetActive(true);
		btnRotate.SetActive(true);
		btnChangeObj.SetActive(false);
		btnDelete.SetActive(true);
	}

	/// <summary>
	/// 2 loai button di chuyen, xoay
	/// </summary>
	public void setDisableButtonAction(){
		btnMove.SetActive(false);
		btnRotate.SetActive(false);
		btnChangeObj.SetActive(false);
		btnDelete.SetActive(false);
	}

	/// <summary>
	/// 2 button config + cancel
	/// </summary>
	public void setActionButtonConfig(){
		btnConfig.SetActive(true);
		btnCancel.SetActive(true);

	}

	/// <summary>
	/// 2 button config + cancel
	/// </summary>
	public void setDisableButtonConfig(){
		btnConfig.SetActive(false);
		btnCancel.SetActive(false);
	}
}
