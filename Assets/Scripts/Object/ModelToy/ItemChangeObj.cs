﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemChangeObj : MonoBehaviour {

	public int id;
	public CanvasObjectSelect canvasModelToy;
	public Image img;
	public Slider slider;

	// Use this for initialization
	void Start () {
	
	}
	void OnEnable(){
		slider.maxValue=Constrains.MAX_ITEM_TOY;
		slider.value=DataAppControl.Instance.getNumber(id);
		img.sprite=DataAppControl.Instance.getSprite(id);

	}
	public void setItem(Sprite sprite,int value){

	}
	public void setModel(){
		//luc click vao scrollview de thay doi model
		if(Utils.objectSelect==id || DataAppControl.Instance.getNumber(id)<=0){
			return;
		}
		Debug.Log("object change "+Utils.objectSelect+" to "+id);
		//tru di so luong hien tai
		DataAppControl.Instance.setNumber(id,DataAppControl.Instance.getNumber(id)-1);
		DataAppControl.Instance.setNumber(Utils.objectSelect,DataAppControl.Instance.getNumber(Utils.objectSelect)+1);

		canvasModelToy.ChangeObj(id);
	}
}
