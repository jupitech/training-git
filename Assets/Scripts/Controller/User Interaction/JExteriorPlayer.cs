﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;


namespace Jupitech.Controllers
{
	public enum InteractType {Independent,Dependent}

    public class JExteriorPlayer : MonoBehaviour
    {

        private static JExteriorPlayer _instance;
        public static JExteriorPlayer Instance
        {
            get
            {
                return _instance;
            }
        }

		public Range zoomRange; //150 -800
		public Range panRange;
//		public Material _skybox;
        private float _currentZoomValue;
		public LayerMask _layerShowRoomMau;
		public LayerMask _seeAll;
		public Transform _PosCameShowRoomMau;
		public float zoomSpeed=10000f;

		public float panSpeed=300f;
		public float rotateSpeed=1f;

        private enum CameraMode { Idle, Panning, Rotating }
        private CameraMode _camMode;

		public bool isOn=true;
        private Transform _camera;
        private Transform _dummyCamera;
		private Transform _PosSave;
        
        private Vector3 _targetCameraPosition;
        private Vector3 _targetDummyPosition;
        private Quaternion _targetDummyQuat;
        private Quaternion _targetCameraQuat;


		float yRot_after=0; //gia tri xrot truoc khi xoay
        private float xRot;
        private float yRot;

		private float xRotLerp;
		private float yRotLerp;

        public Range _cameraRotateRange; //5-75

        [SerializeField]
        private float _effectTime = 1f;

		[SerializeField]
		private bool isPanSupported = true;
		[SerializeField]
		private bool isZoomSupported = true;
		[SerializeField]
		private bool isRotateSupported = true;


#if UNITY_ANDROID || UNITY_IPHONE
        bool isPanning = false;
        int touchZeroID=-1;
        Vector3 panDirection;
        bool isTouch0Clicking = false;
#endif

        void Awake()
        {

			_PosSave = gameObject.transform;

            if (_instance == null)
            {
                _dummyCamera = transform.GetChild(0);
                _camera = _dummyCamera.GetChild(0);
                _instance = this;

            }            
        }


        void Start()
        {
            
            xRot = transform.rotation.eulerAngles.y;
            yRot = _dummyCamera.localRotation.eulerAngles.x;
            defaultXRot = xRot;
            defaultYRot = yRot;
			xRotLerp = xRot;
			yRotLerp = yRot;


            defaultZoomvalue = _camera.localPosition.magnitude;
            defaultTargetDummyPos = transform.position;
            defaultTargetDummyQuart = transform.rotation;
            defaultTargetCameraPos = _camera.localPosition;
            defaultTargetCameraQuart = _dummyCamera.localRotation;



            ResetCamera();

//			ZoomTo(300);

        }

        float defaultZoomvalue;
        Vector3 defaultTargetDummyPos;
        Quaternion defaultTargetDummyQuart;
        Vector3 defaultTargetCameraPos;
        Quaternion defaultTargetCameraQuart;

        float defaultXRot;
        float defaultYRot;

		GameObject posSelect;
        public void ResetCamera()
        {
			//oracle
//			defaultXRot = 36f;
//			defaultYRot = 5f;
//			defaultZoomvalue = 120;
			//

            xRot = defaultXRot;
            yRot = defaultYRot;
            _currentZoomValue = defaultZoomvalue;
            _targetDummyPosition = defaultTargetDummyPos;
            _targetDummyQuat = defaultTargetDummyQuart;
            _targetCameraPosition = defaultTargetCameraPos;
            _targetCameraQuat = defaultTargetCameraQuart;

			//oracle
			ZoomTo(_currentZoomValue);
			RotateView(xRot,yRot);
        }

		public void setGameObjectSelect(GameObject obj){
			if(obj==null){
				MoveTo(Vector3.zero);
			}else{
				posSelect=obj;
				MoveTo(posSelect.transform.position);
			}

		}
        void Update()
        {


			if(isOn){
			if(Utils.stateSelect!=Constrains.STATE_OBJECT_RESET && Utils.stateSelect!=Constrains.STATE_OBJECT3D_CHOISE_BUTTON && Utils.stateSelect!=Constrains.STATE_OBJECT3D_LONGSELECT){
				return;
			}
			#if UNITY_WEBPLAYER  || UNITY_STANDALONE || UNITY_EDITOR || UNITY_WEBGL
            switch (_camMode)
            {
                #region CameraMode_Idle
                case CameraMode.Idle:
                    if (Input.GetMouseButtonDown(1) && isPanSupported)
                    {
                        if (EventSystem.current != null)
                        {
                            if (!EventSystem.current.IsPointerOverGameObject())
                            {
                                _camMode = CameraMode.Panning;                                
                            }
                        }
                        else
                        {
                            _camMode = CameraMode.Panning;                            
                        }
                    }
                    if (Input.GetMouseButtonDown(0) && isRotateSupported)
                    {
                        if (EventSystem.current != null)
                        {
                            if (!EventSystem.current.IsPointerOverGameObject())
                            {
                                _camMode = CameraMode.Rotating;
                            }
                        }
                        else
                        {
                            _camMode = CameraMode.Rotating;
                        }
                    }
                    float x = Input.GetAxis("Mouse ScrollWheel");
                    if (x != 0 && isZoomSupported)
                    {
                        if (EventSystem.current != null)
                        {
                            if (!EventSystem.current.IsPointerOverGameObject())
                            {
                                _currentZoomValue -= x * zoomSpeed * Time.deltaTime;
                                _currentZoomValue = Mathf.Clamp(_currentZoomValue, zoomRange.min, zoomRange.max);
                                _targetCameraPosition = _camera.localPosition.normalized * _currentZoomValue;
								
//							Debug.Log ( " Zoom "+_currentZoomValue);
                            }
                        }
                        else
                        {
                            _currentZoomValue -= x * zoomSpeed * Time.deltaTime;
                            _currentZoomValue = Mathf.Clamp(_currentZoomValue, zoomRange.min, zoomRange.max);
                            _targetCameraPosition = _camera.localPosition.normalized * _currentZoomValue;
                        }
                    }
                    break;
                #endregion
                #region CameraMode_Panning
                case CameraMode.Panning:
                    if (Input.GetMouseButton(1))
                    {
                        float hoz = Input.GetAxis("Mouse X");
                        float ver = Input.GetAxis("Mouse Y");
						

                        _targetDummyPosition -= (hoz * transform.right + ver * transform.forward).normalized * panSpeed * Time.deltaTime; 
                        //clamp something here 
//						_targetDummyPosition = Mathf.Clamp(_targetDummyPosition, panRange.min, panRange.max);
                    }

                    if (Input.GetMouseButtonUp(1))
                    {
                        _camMode = CameraMode.Idle;
                    }
                    break;
                #endregion
                #region CameraMode_Rotating
                case CameraMode.Rotating:
                    if (Input.GetMouseButton(0))
                    {                        
                        xRot += Input.GetAxis("Mouse X") * rotateSpeed * Time.deltaTime;
                        yRot -= Input.GetAxis("Mouse Y") * rotateSpeed * Time.deltaTime;

                        yRot = Mathf.Clamp(yRot, _cameraRotateRange.min, _cameraRotateRange.max);
                        _targetCameraQuat = Quaternion.Euler(yRot, _targetCameraQuat.eulerAngles.y, 0);
                        _targetDummyQuat = Quaternion.Euler(_targetDummyQuat.eulerAngles.x, xRot, 0);
					//_targetDummyQuat *= Quaternion.Euler(0,Input.GetAxis("Mouse X") * rotateSpeed * Time.deltaTime,0);
                    }

                    if (Input.GetMouseButtonUp(0))
                    {
                        _camMode = CameraMode.Idle;
                    }
                    break;
                #endregion
            }
				#elif UNITY_ANDROID || UNITY_IPHONEtrue
         if (Input.touchCount > 0)
			{
				switch(Input.touchCount)
				{
				case 1:
					Touch zero = Input.GetTouch(0);
					switch (zero.phase)
					{
					case TouchPhase.Began:
						if (touchZeroID == -1 && !isTouch0Clicking)
						{
                            if (EventSystem.current != null)
                            {
                                if (!EventSystem.current.IsPointerOverGameObject(zero.fingerId))
                                {
                                    touchZeroID = zero.fingerId;
                                    isTouch0Clicking = true;
//                                    EventManager.Instance.eventOnMouseDown.InvokeEvent(zero.position);
                                }
                            }
                            else
                            {
                                touchZeroID = zero.fingerId;
                                isTouch0Clicking = true;
//                                EventManager.Instance.eventOnMouseDown.InvokeEvent(zero.position);
                            }
				            
						}
						break;
					case TouchPhase.Stationary:
					case TouchPhase.Moved:
						if (isTouch0Clicking)
						{
							//float amount = zero.deltaPosition.magnitude;
							//amount = zero.deltaPosition.x < 0 ? -amount : amount;
							//using Rotate with one touch on Ipad
							OnRotateContinue(zero.deltaPosition.normalized * 0.75f);
						}
						break;
					case TouchPhase.Ended:
						if (isTouch0Clicking && touchZeroID != -1)
						{
							isTouch0Clicking = false;
                            if (touchZeroID == zero.fingerId)
                            {
//                                EventManager.Instance.eventOnMouseUp.InvokeEvent(zero.position);
                            }
							touchZeroID = -1;                                                       
						}
						break;
					}
					break;
				case 2:
					
					touchZeroID = -1;
					isTouch0Clicking = false;
					
					Touch touchZero = Input.GetTouch(0);
					Touch touchOne = Input.GetTouch(1);

                    if (touchZero.phase == TouchPhase.Moved || touchOne.phase == TouchPhase.Moved)
                    {


                        Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                        Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                        float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                        float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                        float deltaMagnitudediff = prevTouchDeltaMag - touchDeltaMag;

                        OnZoomContinue(- deltaMagnitudediff / 7.5f);
                    }
					break;
				}
			} 
#endif
			}
        }

        void LateUpdate()
        {

			if(isOn){
            _camera.localPosition = Vector3.Lerp(_camera.localPosition, _targetCameraPosition, _effectTime);
			transform.position = Vector3.Lerp(transform.position, _targetDummyPosition, _effectTime);

			xRotLerp = Mathf.Lerp(xRotLerp,xRot,_effectTime);
			yRotLerp = Mathf.Lerp(yRotLerp,yRot,_effectTime);

			_dummyCamera.localRotation = Quaternion.Euler(yRotLerp,_dummyCamera.localEulerAngles.y,0);
			transform.localRotation = Quaternion.Euler(transform.localEulerAngles.x,xRotLerp,0);

			//_dummyCamera.localRotation = Quaternion.Lerp(_dummyCamera.localRotation, _targetCameraQuat, _effectTime);
			//transform.rotation = Quaternion.Lerp(transform.rotation, _targetDummyQuat, _effectTime);
		}
		}
		[ContextMenu("move to")]
		public void testmove(){
			MoveTo(new Vector3(1200,10,10));
		}
        public void MoveTo(Vector3 position)
        {
            //_dummyCamera.transform.position = position;
			_targetDummyPosition = position;
        }

        public void MoveTo(Vector3 position, float zoomLevel)
        {
            zoomLevel = Mathf.Clamp(zoomLevel, 0f, 20f);
            _currentZoomValue = zoomLevel * (zoomRange.max - zoomRange.min) / 20f;
            _targetCameraPosition = _camera.localPosition.normalized * _currentZoomValue;
        }
		public void ZoomTo(float currentZoom){
			_currentZoomValue=currentZoom;
			_targetCameraPosition = _camera.localPosition.normalized * _currentZoomValue;
		}
		public void RotateView(float xRotate,float yRotate){

			this.xRot=xRotate;
			this.yRot=yRotate;
			_targetCameraQuat = Quaternion.Euler(yRot, _targetCameraQuat.eulerAngles.y, 0);
			_targetDummyQuat = Quaternion.Euler(_targetDummyQuat.eulerAngles.x, xRot, 0);
		}

        void OnPanFinished(Vector3 v3)
        {

        }

        void OnRotateContinue(Vector3 v)
        {
            if (isRotateSupported)
            {
                xRot += v.x * rotateSpeed * Time.deltaTime;
                yRot -= v.y * rotateSpeed * Time.deltaTime;

                yRot = Mathf.Clamp(yRot, _cameraRotateRange.min, _cameraRotateRange.max);
				_targetCameraQuat = Quaternion.Euler(yRot, _targetCameraQuat.eulerAngles.y, 0);
				_targetDummyQuat = Quaternion.Euler(_targetDummyQuat.eulerAngles.x, xRot, 0);
				
			}
		}
		
		void OnPanContinue(Vector3 direction)
        {
            if (isPanSupported)
            {                
                _targetDummyPosition -= (direction.x * transform.right + direction.y * transform.forward).normalized * panSpeed * Time.deltaTime;
            }
        }

        void OnZoomContinue(float f)
        {
            if (isZoomSupported)
            {
                _currentZoomValue -= f * zoomSpeed * Time.deltaTime;
                _currentZoomValue = Mathf.Clamp(_currentZoomValue, zoomRange.min, zoomRange.max);
                _targetCameraPosition = _camera.localPosition.normalized * _currentZoomValue;
            }
        }

        //Extends

        public float roomZoomLevel;

        bool isSaved;
        // cac ham dieu khien goc nhin camera

        public void CamSeeAll() 
        {

            RotateView(xRot, 90f);
			ZoomTo(100);
//            OnZoomContinue(50f);
        }
        public void CamSeeDown()
        {
            RotateView(xRot, 20f);
            if(_currentZoomValue>=zoomRange.max){
				ZoomTo((zoomRange.max-zoomRange.min)/2);
			}
        }

    }

   
    



}
