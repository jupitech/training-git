﻿using UnityEngine;
using System.Collections;

public class AnimPanelControl : MonoBehaviour {

	public Animator animator;
	// Use this for initialization
	void Start () {
	
	}
	
	public void showPanel(){
		animator.SetBool("state",true);
	}
	public void hidePanel(){
		animator.SetBool("state",false);
	}
}
