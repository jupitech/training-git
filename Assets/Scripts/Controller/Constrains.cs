﻿using UnityEngine;
using System.Collections;

public class Constrains {
	
	//INTERIOR SETTINGS
	//------------------------------------------------
	public const float PLAYER_MOVE_SPEED_MIN=10F;
	public const float PLAYER_MOVE_SPEED_MAX=20F;
	public const float PLAYER_MOVE_SPEED_CURRENT=15F;
	
	public const float PLAYER_ROTATION_SPEED_MIN=120F;
	public const float PLAYER_ROTATION_SPEED_MAX=210F;
	public const float PLAYER_ROTATION_SPEED_CURRENT=160F;
	
	//EXTERIOR SETTINGS
	public const float CAMERA_PAN_SPEED_MIN=-0.01F;
	public const float CAMERA_PAN_SPEED_MAX=0F;
	public const float CAMERA_PAN_SPEED_CURRENT=0F;
	
	public const float CAMERA_ROTATE_SPEED_MIN=-0.01F;
	public const float CAMERA_ROTATE_SPEED_MAX=0F;
	public const float CAMERA_ROTATE_SPEED_CURRENT=0F;
	
	public const float CAMERA_ZOOM_SPEED_MIN=-0.01F;
	public const float CAMERA_ZOOM_SPEED_MAX=0F;
	public const float CAMERA_ZOOM_SPEED_CURRENT=0F;

	//project punco
	public const int STATE_OBJECT_RESET=0; //trang thai dung yen
	public const int STATE_OBJECT2D_SELECT=1;

	public const int STATE_OBJECT3D_MOVE=2; 

	public const int STATE_OBJECT3D_LONGSELECT=5; //luc long click
	public const int STATE_OBJECT3D_SELECT_MOVE=3; //luc move
	public const int STATE_OBJECT3D_SELECT_ROTATE=4; //luc xoay
	public const int STATE_OBJECT3D_CHOISE_BUTTON=6; //luc dang chon move hoac xoay

	public const int MAX_ITEM_TOY=9;

	public const string TAG_MODEL_TOY="ModelToy";

	public const string NAME_UI_MOVE="ButtonMove";
	public const string NAME_UI_ROTATE="ButtonRotate";
	public const string NAME_UI_CONFIG="ButtonConfig";
	public const string NAME_UI_CANCEL="ButtonCancel";

	public const int MAX_VALUE_ITEM=10;
	
	
	
}


