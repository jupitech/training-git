﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScrollViewControl : MonoBehaviour {


	public PanelGroupItem panelGroupItem;

	ScrollRect scrollview;
	public Scrollbar scrollBar;

	// Use this for initialization
	void Start () {
		scrollview=gameObject.GetComponent<ScrollRect>();
	}
	
	public void setScrollViewEnable(){
		scrollview.enabled=true;
	}
	public void setScrollViewDisable(){
		scrollview.enabled=false;
	}
	public void button_Next(){
		scrollBar.value=1;
	}
	public void button_Back(){
		scrollBar.value=0;
	}

}
