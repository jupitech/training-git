﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class PanelGroupItem : MonoBehaviour,IPointerExitHandler {
	

	public Object3DControl object3DControl;
	public ScrollViewControl scrollViewControl;
	ItemCanvasHouse[] lstItemCavasHouse;

	public GameObject ItemTop0;

	// Use this for initialization
	void Start () {
		lstItemCavasHouse=gameObject.GetComponentsInChildren<ItemCanvasHouse>();
	}
	
	#region IPointerExitHandler implementation

	public void OnPointerExit (PointerEventData eventData)
	{
		if(Utils.stateSelect==Constrains.STATE_OBJECT2D_SELECT){

//			Utils.stateSelect=Constrains.STATE_OBJECT3D_MOVE;
			scrollViewControl.setScrollViewEnable();

//			Debug.Log("id select "+Utils.objectSelect);
		}
	}

	#endregion
	
	public void ReloadNumItemCanvasToy(){
		foreach(ItemCanvasHouse item in lstItemCavasHouse){
			item.updateNumberToy();
		}
	}
}
