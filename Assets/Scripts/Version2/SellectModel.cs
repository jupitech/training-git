using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class SellectModel : MonoBehaviour {

    public LayerMask _layerCast;
    public Camera _cam;

    [SerializeField]
    private List<GameObject> _listSelect;
    void Start()
    {
        _listSelect = new List<GameObject>();
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = _cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, _layerCast))
            {

                Checklist(hit.collider.gameObject);
                Debug.Log(hit.collider.gameObject.name);


            }
        }
    }

    // check xem model day da co trong list chua
    private void  Checklist(GameObject model)
    {
        if (_listSelect != null)
        {
            int countList = _listSelect.Count;
            for (int i = 0; i < countList; i++)
            {
                if (model.name == _listSelect[i].name)
                {
                    return;
                }
            }

            _listSelect.Add(model);
        }
    }
}
