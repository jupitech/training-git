﻿using UnityEngine;
using System.Collections;
using Jupitech.Controllers;
public class ButtonController : MonoBehaviour {

    private JExteriorPlayer _camera; // camera chinh
    [SerializeField]
    private GameObject _panleSample;
    private bool isShowSubmit;
	public bool isShowSample;
    void Awake()
    {
        _camera = FindObjectOfType<JExteriorPlayer>();

    }
    public void ButtonSeeAll()
    {
		_camera.CamSeeAll();
    }
    public void ButtonSeePlant()
    {

        _camera.CamSeeDown();
    }
    public void ButtonReset()
    {
		ModelToyObject[] lstModel =GameObject.FindObjectsOfType<ModelToyObject>();

		if (lstModel.Length > 0)
        {
            foreach(ModelToyObject gtemp in lstModel){
				Destroy(gtemp.gameObject);

			}
        }

		DataAppControl.Instance.reloadNumber();
		reloadInitScrollUI();

    }
    public void ButtonSubmit()
    {
        
        isShowSubmit = !isShowSubmit;
        if (isShowSubmit)
            Godthanh.Instance.SubmitPanle.ShowPanleSubmit();
        else
            Godthanh.Instance.SubmitPanle.HidenPanleSubmit();
    }
    public void Confirm()
    {
		isShowSubmit=false;
        Godthanh.Instance.SubmitPanle.HidenPanleSubmit();
    }
    public void Sample()
    {
		isShowSample=!isShowSample;
		if(isShowSample)
        _panleSample.SetActive(true);
		else
			_panleSample.SetActive(false);
    }
	void reloadInitScrollUI(){
		//update so luong
		ItemCanvasHouse[] lstItemCanvas=GameObject.FindObjectsOfType<ItemCanvasHouse>();
		foreach(ItemCanvasHouse item in lstItemCanvas){
			item.updateNumberToy();
		}
	}
     
}
