﻿using UnityEngine;
using System.Collections;

public class StateObject : MonoBehaviour {

	public GameObject stateMove,stateRotate;
	// Use this for initialization
	void Start () {
	
	}
	public void setStateMove(){
		stateMove.SetActive(true);
	}
	public void setStateRotate(){
		stateRotate.SetActive(true);
	}
	public void setDisableState(){
		stateMove.SetActive(false);
		stateRotate.SetActive(false);
	}

}
