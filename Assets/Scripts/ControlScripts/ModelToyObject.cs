﻿using UnityEngine;
using System.Collections;

public class ModelToyObject : MonoBehaviour {

	public int ID=-1;
	CanvasObjectSelect canvasObjectSelect;

	Vector3 initMove=Vector3.zero; //vector vi tri sau khi tra ve cancel su kien
	Quaternion initRotate=Quaternion.identity; //vector vi tri sau khi tra ve cancel su kien

	bool actionForcus=false;
	bool actionForcusMove=false;
	bool actionForcusRotate=false;

	// Use this for initialization
	void Start () {
		canvasObjectSelect=GameObject.FindObjectOfType<CanvasObjectSelect>();
	}
	void OnEnable(){
		if(canvasObjectSelect==null){
			canvasObjectSelect=GameObject.FindObjectOfType<CanvasObjectSelect>();
		}
	}

	public bool ActionForcus{
		get{
			return actionForcus;
		}
		set{

			actionForcus=value;
			if(actionForcus){
				Utils.objectSelect=ID;
			}

		}
	}
	public bool ActionForcusMove{
		get{
			return actionForcusMove;
		}
		set{
			actionForcusMove=value;
		}
	}
	public bool ActionForcusRotate{
		get{
			return actionForcusRotate;
		}
		set{
			actionForcusRotate=value;
		}
	}
	public void setInitMove(){
		initMove=transform.position;
	}
	public void setInitRotate(){
		initRotate=transform.rotation;
	}
	public void loadInitMove(){
		transform.position=initMove;
	}
	public void loadInitRotate(){
		transform.rotation=initRotate;
	}

	void Update(){
		if(actionForcus){
			Debug.Log("actionForcus "+gameObject.name);
		}
	}
	//slider bar
	public void ActionForcusModel(){
		Utils.stateSelect=Constrains.STATE_OBJECT3D_CHOISE_BUTTON;
		actionForcus=true;
		canvasObjectSelect.setActionModelObject(this);

		canvasObjectSelect.actionUI();

	}
	public void StopSliderBar(){
		actionForcus=false;
//		Utils.stateSelect=Constrains.STATE_OBJECT_RESET;
		canvasObjectSelect.setDisable();
	}

	public void RotateLeft(bool rotate,float range){
		range*=-1;
		if(actionForcus){
			if(rotate){
				transform.Rotate(new Vector3(0,range,0));
			}else{
				transform.Rotate(new Vector3(0,range,0));
			}
		}


	}
	public void Move(Vector3 pos){
//		Debug.Log("action forcus "+actionForcus);
		if(actionForcus){
			transform.position=pos;
		}

	}
	public void ChangeObjModel(int id){
		//trao doi so luong

		reloadScrollUI();

		Instantiate(DataAppControl.Instance.getModel(id),transform.position,transform.rotation);
		Destroy(gameObject);

	}
	public void DeleteModel(){

		reloadScrollUI();
		Destroy(gameObject);
	}
	void reloadScrollUI(){
		//update so luong
		ItemCanvasHouse[] lstItemCanvas=GameObject.FindObjectsOfType<ItemCanvasHouse>();
		foreach(ItemCanvasHouse item in lstItemCanvas){
			item.updateNumberToy();
		}
	}


}
