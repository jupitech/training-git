﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
//using Jupitech.Events;

namespace Jupitech.Controllers
{
    public enum JoystickType { Moving, Rotating }

    public class JoystickPad : MonoBehaviour, IDragHandler , IBeginDragHandler , IEndDragHandler 
    {                       
        [SerializeField]
        private JoystickType type;

        public JInteriorPlayer player;

        private Vector2 storedVector2;

        float deadDistance=10f;        

        public void OnDrag(PointerEventData eventData)
        {            
            Vector2 diff = eventData.position - storedVector2;
            if (diff.magnitude < deadDistance)
            {
                diff = Vector2.zero;
            }

            switch (type)
            {
                case JoystickType.Moving:
					Debug.Log("move");
//                    player.SetMoveDirection(diff);
                    break;
                case JoystickType.Rotating:
					Debug.Log("rotate");
//                    player.SetRotateDirection(diff);
                    break;
            }            
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            storedVector2 = eventData.position;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            switch (type)
            {
                case JoystickType.Moving:
//                    player.SetMoveDirection(Vector2.zero);
                    break;
                case JoystickType.Rotating:
//                    player.SetRotateDirection(Vector2.zero);
                    break;
            }
            
        }

    }
}