﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

namespace Jupitech.Controllers
{
	//public enum InteractType { Independent, Dependent }
	public class JInteriorPlayer : MonoBehaviour
	{
		public InteractType _type;
		
		public float moveSpeed; //15
		
		public float rotateSpeed; //60
		
		public bool isInverted=false;
		
		private float clampAngleMin;
		private float clampAngleMax;
		
		private float xRot;
		private float yRot;
		
		private Transform _transform;
		private Rigidbody _rigidbody;
		
		private CapsuleCollider col;
		
		private Vector3 castOffset = new Vector3(0,0.1f,0);
		
		//[SerializeField]
		//private float castDistance = 0.2f;
		
		
		public float clampXMax=45; //45
		
		public float clampXMin=-45; //-45
		
		
		
		void Awake()
		{
			_transform = transform;
			_rigidbody = GetComponent<Rigidbody>();
			col = GetComponent<CapsuleCollider>();
			castOffset.y = col.height / 2 * _transform.localScale.y;           
		}
		
		void Start()
		{
			//if (_type == InteractType.Dependent)
			//{
			//    _navControl = NavigationManager.Instance;
			//    if (_navControl == null)
			//    {
			//        _type = InteractType.Independent;
			//    }
			//}
			_camera = _transform.GetChild(0);
			xRot = _transform.localEulerAngles.y;
			yRot = _camera.localEulerAngles.x;
//			#if UNITY_ANDROID || UNITY_IPHONE
			bottomLeftRect = new Rect(0, 0 * Screen.height, 0.5f * Screen.width, 0.5f * Screen.height);
			bottomRightRect = new Rect(0.5f*Screen.width, 0 * Screen.height, 0.5f * Screen.width, 0.5f * Screen.height);
			maxMoveTouchRatio = Mathf.Clamp(maxMoveTouchRatio, 0, 1);
			
			maxMoveTouchRatio *= Mathf.Sqrt(Screen.width * Screen.width + Screen.height + Screen.height);
//			#endif
			
		}
		
		
		private float _axisX;
		private float _axisY;
		private float _magnitude;
		
		private Transform _camera;
		
		bool isRotate;
		
//		#if UNITY_ANDROID || UNITY_IPHONE
		private Rect bottomLeftRect;
		private Rect bottomRightRect;
		private int leftID = -1;
		private int rightID = -1;
		private Vector3 leftStoredVector2;
		private Vector3 rightStoredVector2;
		public float maxMoveTouchRatio = 0.1f;
//		#endif
		
		void Update()
		{
			#if UNITY_WEBPLAYER || UNITY_STANDALONE || UNITY_EDITOR
			
			//if (Input.GetAxis("Joystick Right X") == 0 && Input.GetAxis("Joystick Right Y") == 0)
			if (Input.GetMouseButtonDown(0))
			{
				if (_type == InteractType.Dependent)
				{
					if (EventSystem.current != null)
					{
						if (!EventSystem.current.IsPointerOverGameObject())
						{
							isRotate = true;
						}
					}
					else
					{
						isRotate = true;
					}
				}
				else
				{
					isRotate = true;
				}
			}
			
			if (isRotate && Input.GetMouseButton(0))
			{
				xRot += Input.GetAxis("Mouse X") * rotateSpeed * Time.deltaTime;
				if (isInverted)
				{
					yRot += Input.GetAxis("Mouse Y") * rotateSpeed * Time.deltaTime;
				}
				else
				{
					yRot -= Input.GetAxis("Mouse Y") * rotateSpeed * Time.deltaTime;
				}
				yRot = Mathf.Clamp(yRot, clampXMin, clampXMax);
				
				_transform.rotation = Quaternion.Euler(0, xRot, 0);
				_camera.localRotation = Quaternion.Euler(yRot, 0, 0);
			}
			
			if (Input.GetMouseButtonUp(0))
			{
				isRotate = false;
			}
			/*
            else
            {
                xRot += Input.GetAxis("Joystick Right X") * rotateSpeed * Time.deltaTime;
                if (isInverted)
                {
                    yRot += Input.GetAxis("Joystick Right Y") * rotateSpeed * Time.deltaTime;
                }
                else
                {
                    yRot -= Input.GetAxis("Joystick Right Y") * rotateSpeed * Time.deltaTime;
                }
                yRot = Mathf.Clamp(yRot, clampXMin, clampXMax);

                _transform.rotation = Quaternion.Euler(0, xRot, 0);
                _camera.localRotation = Quaternion.Euler(yRot, 0, 0);
            }
            */
			#elif UNITY_ANDROID || UNITY_IPHONE
			//simulate touch
			
			if (Input.touchCount > 0 && Input.touchCount < 3)
			{
				Touch refTouch;
				
				for (int i = 0; i < Input.touchCount; i++)
				{
					refTouch = Input.GetTouch(i);
					switch (refTouch.phase)
					{
					case TouchPhase.Began:
						if (leftID == -1)
						{
							if (bottomLeftRect.Contains(refTouch.position))
							{
								leftID = refTouch.fingerId;
								leftStoredVector2 = refTouch.position;
							}
						}
						if (rightID == -1)
						{
							if (bottomRightRect.Contains(refTouch.position))
							{
								rightID = refTouch.fingerId;
								rightStoredVector2 = refTouch.position;
							}
						}
						break;
					case TouchPhase.Stationary:
					case TouchPhase.Moved:
						if (leftID !=-1 && refTouch.fingerId == leftID)
						{
							Vector2 diff = new Vector2(refTouch.position.x - leftStoredVector2.x, refTouch.position.y - leftStoredVector2.y);
							SetMoveDirection(diff);
						}
						if (rightID != -1 && refTouch.fingerId == rightID)
						{
							Vector2 diff = new Vector2(refTouch.position.x - rightStoredVector2.x, refTouch.position.y - rightStoredVector2.y);
							SetRotateDirection(diff);
						}
						break;
					case TouchPhase.Canceled:
					case TouchPhase.Ended:
						if (leftID !=-1 && refTouch.fingerId == leftID)
						{
							leftID = -1;
							SetMoveDirection(Vector3.zero);
						}
						if (rightID != -1 && refTouch.fingerId == rightID)
						{
							rightID = -1;
							SetRotateDirection(Vector3.zero);
						}
						break;
					}
				}
			}
			else
			{
				leftID = -1;
				SetMoveDirection(Vector3.zero);
				rightID = -1;
				SetRotateDirection(Vector3.zero);
			}
			
			// Physics.Raycast(
			
			//// touch began
			// if (Input.GetMouseButtonDown(0))
			// {
			//     Vector3 convertInput = Input.mousePosition;
			//     convertInput.y *= -1;
			//     if (bottomLeftRect.Contains(Input.mousePosition))
			//     {                    
			//         leftID = 1;
			//         leftStoredVector2 = Input.mousePosition;
			//     }
			// }
			
			// if (leftID != -1)
			// {
			//     if (Input.GetMouseButton(0))
			//     {
			//         Vector2 diff = new Vector2(Input.mousePosition.x - leftStoredVector2.x, Input.mousePosition.y - leftStoredVector2.y);
			//         SetRotateDirection(diff);
			//     }
			
			//     if (Input.GetMouseButtonUp(0))
			//     {
			//         leftID = -1;
			//         SetRotateDirection(Vector3.zero);
			//     }
			// }
			#endif
		}
		
		#if UNITY_ANDROID || UNITY_IPHONE
		
		Vector2 moveVector;
		Vector2 rotateVector;
		
		public void SetMoveDirection(Vector2 moveDirection)
		{
			//this.moveVector = moveDirection/maxMoveTouchRatio;
			//this.moveVector.x = Mathf.Clamp(this.moveVector.x, -1, 1);
			//this.moveVector.y = Mathf.Clamp(this.moveVector.y, -1, 1);
			this.moveVector = moveDirection.normalized;
		}
		
		public void SetRotateDirection(Vector2 rotateDirection)
		{
			//this.rotateVector = rotateDirection.normalized * 0.5f;
			//this.rotateVector.x = rotateDirection.x / Screen.width;
			//this.rotateVector.y = rotateDirection.y / Screen.height;
			//this.rotateVector = rotateDirection/ maxMoveTouchRatio;
			//this.rotateVector.x = Mathf.Clamp(this.rotateVector.x, -1, 1);
			//this.rotateVector.y = Mathf.Clamp(this.rotateVector.y, -1, 1);
			//this.rotateVector = rotateDirection.normalized;
			this.rotateVector = rotateDirection / maxMoveTouchRatio;
			if (this.rotateVector.x > 1)
			{
				this.rotateVector.x = 1;
			}
			else if( this.rotateVector.x < -1)
			{
				this.rotateVector.x = -1;
			}
			
			if (this.rotateVector.y > 1)
			{
				this.rotateVector.y = 1;
			}
			else if (this.rotateVector.y < -1)
			{
				this.rotateVector.y = -1;
			}
		}
		#endif
		
		void FixedUpdate()
		{
			#if UNITY_WEBPLAYER || UNITY_STANDALONE //|| UNITY_EDITOR
			_axisX = Input.GetAxis("Horizontal");
			_axisY = Input.GetAxis("Vertical");
			Vector3 direction = _transform.forward * _axisY + _transform.right * _axisX;
			
			//_rigidbody.velocity = direction.normalized * moveSpeed;
			
			_rigidbody.MovePosition(_rigidbody.position + direction.normalized * moveSpeed * Time.deltaTime);
			
			//_magnitude = direction.sqrMagnitude;
			//if (_axisX != 0 || _axisY != 0)
			//{
			//    if (_magnitude > 1)
			//    {
			//        direction.Normalize();
			//    }
			//    //checking if anything in the raycast
			//    Vector3 distanceTravel = direction * moveSpeed * Time.fixedDeltaTime;
			//    Vector3 castPoint1 = _rigidbody.position + castOffset;
			//    Vector3 castPoint2 = castPoint1 + castDistance * direction.normalized;
			//    if (!Physics.Linecast(castPoint1, castPoint2))
			//    {
			//        _rigidbody.MovePosition(_rigidbody.position + distanceTravel);
			//    }                
			//}
			//else
			//{
			//    _rigidbody.velocity = Vector3.zero;
			//}
			#elif UNITY_ANDROID || UNITY_IPHONE
			//if (Input.touchCount <= 0)
			//{
			//    leftTouchID = -1;
			//    rightTouchID = -1;
			//    return;
			//}
			//else
			//{
			//    Touch refTouch;
			//    for (int i = 0; i < Input.touchCount; i++)
			//    {
			//        refTouch = Input.GetTouch(i);
			//        switch (refTouch.phase)
			//        {
			//            case TouchPhase.Began:
			
			//                break;
			//            case TouchPhase.Canceled:
			//            case TouchPhase.Ended:
			
			//                break;
			//            case TouchPhase.Moved:
			
			//                break;
			//        }
			//    }
			//}
			Vector3 direction = _transform.forward * moveVector.y + _transform.right * moveVector.x;
			
			_rigidbody.MovePosition(_rigidbody.position + direction.normalized * moveSpeed * Time.deltaTime);
			
			xRot += rotateVector.x * rotateSpeed * Time.deltaTime;
			if (isInverted)
			{
				yRot += rotateVector.y * rotateSpeed * Time.deltaTime;
			}
			else
			{
				yRot -= rotateVector.y * rotateSpeed * Time.deltaTime;
			}
			yRot = Mathf.Clamp(yRot, clampXMin, clampXMax);
			
			_transform.rotation = Quaternion.Euler(0, xRot, 0);
			_camera.localRotation = Quaternion.Euler(yRot, 0, 0);
			
			#endif
		}
		
		//void OnGUI()
		//{
		//    GUI.Box(bottomLeftRect, "");
		//}
	}
}
